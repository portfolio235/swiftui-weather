//
//  WeatherDayView.swift
//  SwiftUI-Weather
//
//  Created by Massimiliano Bonafede on 05/09/22.
//

import SwiftUI

struct WeatherDayView: View {
    var day: String
    var image: String
    var temperature: Int
    
    var body: some View {
        VStack {
            Text(day)
                .font(.system(size: 16, weight: .medium))
                .foregroundColor(.white)
            Image(systemName: image)
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 40, height: 40)
            Text("\(temperature)°")
                .font(.system(size: 28, weight: .medium))
                .foregroundColor(.white)
        }
    }
}
