//
//  ButtonDay.swift
//  SwiftUI-Weather
//
//  Created by Massimiliano Bonafede on 05/09/22.
//

import SwiftUI

struct ButtonDay: View {
    var title: String
    var completion: (() -> Void)
    
    var body: some View {
        Button {
            print("Button tapped")
            completion()
        } label: {
            Text(title)
                .font(.system(size: 17, weight: .bold))
                .frame(width: 200, height: 50)
                .background(.white)
                .foregroundColor(.blue)
                .cornerRadius(10.0)
        }
    }
}
