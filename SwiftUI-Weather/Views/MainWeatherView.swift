//
//  MainWeatherView.swift
//  SwiftUI-Weather
//
//  Created by Massimiliano Bonafede on 05/09/22.
//

import SwiftUI

struct MainWeatherView: View {
    @Binding var isNight: Bool
    var temperature: Int
    
    private var image: String {
        return isNight ? "moon.stars.fill" : "cloud.sun.fill"
    }
    
    
    var body: some View {
        VStack(spacing: 10) {
            Image(systemName: image)
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 180, height: 180)
            
            Text("\(temperature)°")
                .font(.system(size: 70, weight: .medium))
                .foregroundColor(.white)
        }
        .padding(.bottom, 40)
    }
}
