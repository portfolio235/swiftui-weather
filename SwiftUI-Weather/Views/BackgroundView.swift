//
//  BackgroundView.swift
//  SwiftUI-Weather
//
//  Created by Massimiliano Bonafede on 05/09/22.
//

import SwiftUI

struct BackgroundView: View {
    @Binding var isNight: Bool
    
    private var topColor: Color {
        return isNight ? .black : .blue
    }
    
    private var bottom: Color {
        return isNight ? .gray : Color("lightBlue")
    }
    
    var body: some View {
        LinearGradient(
            gradient: Gradient(
                colors: [topColor, bottom]),
            startPoint: .topLeading,
            endPoint: .bottomTrailing)
        .edgesIgnoringSafeArea(.all)
    }
}
