//
//  TitleView.swift
//  SwiftUI-Weather
//
//  Created by Massimiliano Bonafede on 05/09/22.
//

import SwiftUI

struct TitleView: View {
    var title: String
    
    var body: some View {
        Text(title)
            .font(.system(
                size: 32,
                weight: .medium,
                design: .default))
            .foregroundColor(.white)
            .padding()
    }
}
