//
//  ContentView.swift
//  SwiftUI-Weather
//
//  Created by Massimiliano Bonafede on 05/09/22.
//

import SwiftUI

struct ContentView: View {
    @State private var isNight: Bool = false
    
    var body: some View {
        ZStack {
            BackgroundView(isNight: $isNight)
            
            VStack {
                TitleView(title: "Cupertino, CA")
                MainWeatherView(
                    isNight: $isNight,
                    temperature: 35)
                HStack(spacing: 20) {
                    WeatherDayView(day: "MON", image: "cloud.sun.fill", temperature: 33)
                    WeatherDayView(day: "TUE", image: "cloud.sun.bolt.fill", temperature: 13)
                    WeatherDayView(day: "WED", image: "sun.max.fill", temperature: 40)
                    WeatherDayView(day: "THU", image: "wind.snow", temperature: 01)
                    WeatherDayView(day: "FRI", image: "tornado", temperature: 17)
                }
                
                Spacer()
                
                ButtonDay(title: "Change Day Time") {
                    isNight.toggle()
                }

                Spacer()
                LoaderView {
                    print("Hello")
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct LoaderView: View {
    var completion: (() -> Void)
    
    var body: some View {
        ProgressView()
            .tint(.red)
            .scaleEffect(2)
            .progressViewStyle(.circular)
            .onTapGesture {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    completion()
                }
            }
    }
}
